//
//  AppDelegate.swift
//  VZM_iOS_Client
//
//  Created by Anubhav on 31/12/2021.
//

import UIKit
import Flutter
import FlutterPluginRegistrant
import Contacts

@UIApplicationMain
class AppDelegate: FlutterAppDelegate {

   // lazy var flutterEngine = FlutterEngine(name: "my flutter engine")
    let engines = FlutterEngineGroup(name: "multiple-flutters", project: nil)
    override func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Runs the default Dart entrypoint with a default Flutter route.
       // flutterEngine.run();

      //  GeneratedPluginRegistrant.register(with: self.flutterEngine);
        
        let tabBarController = TabBarController()
        tabBarController.tabBar.isTranslucent = false
        self.window?.rootViewController = tabBarController
        self.window?.makeKeyAndVisible()
        self.requestAccess { accessGranted in
            
        }
        return super.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    func requestAccess(completionHandler: @escaping (_ accessGranted: Bool) -> Void) {
        let contactStore = CNContactStore()
        switch CNContactStore.authorizationStatus(for: .contacts) {
        case .authorized:
            completionHandler(true)
        case .denied:
            showSettingsAlert(completionHandler)
        case .restricted, .notDetermined:
            contactStore.requestAccess(for: .contacts) { granted, error in
                if granted {
                    completionHandler(true)
                } else {
                    DispatchQueue.main.async {
                        self.showSettingsAlert(completionHandler)
                    }
                }
            }
        @unknown default:
            break
        }
    }

    private func showSettingsAlert(_ completionHandler: @escaping (_ accessGranted: Bool) -> Void) {
        let alert = UIAlertController(title: nil, message: "This app requires access to Contacts to proceed. Go to Settings to grant access.", preferredStyle: .alert)
        if
            let settings = URL(string: UIApplication.openSettingsURLString),
            UIApplication.shared.canOpenURL(settings) {
                alert.addAction(UIAlertAction(title: "Open Settings", style: .default) { action in
                    completionHandler(false)
                    UIApplication.shared.open(settings)
                })
        }
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { action in
            completionHandler(false)
        })
        self.window.rootViewController?.present(alert, animated: true, completion: nil)
    }

    
}
