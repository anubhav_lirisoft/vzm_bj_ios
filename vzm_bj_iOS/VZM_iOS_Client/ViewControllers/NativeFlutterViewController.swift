//
//  NativeFlutterViewController.swift
//  MessagePOC
//
//  Created by Lirisoft on 18/01/22.
//

import Foundation
import UIKit
import Flutter
import FlutterPluginRegistrant
class NativeFlutterViewController: FlutterViewController {
    private var channel: FlutterMethodChannel?
   
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    init(withEntrypoint entryPoint: String?) {
        let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let route = "/" + (entryPoint ?? "")
        let newEngine = appDelegate.engines.makeEngine(withEntrypoint: nil, libraryURI: nil, initialRoute: route)
        GeneratedPluginRegistrant.register(with: newEngine)
        super.init(engine: newEngine, nibName: nil, bundle: nil)
    }
    
    deinit {
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
