//
//  FlutterChannelManager.swift
//  FlutterModuleInNativeApp
//
//  Created by Anubhav on 17/09/2021.
//

import UIKit
import Flutter

class FlutterChannelManager {
    static let shared = FlutterChannelManager()
    var flutterChannel : FlutterMethodChannel!
    private let channelName = "CHAT_SDK_CHANNEL"
    private init(){}
    
    func initializeChannel(forVC vc: FlutterViewController){
        flutterChannel = FlutterMethodChannel(name: channelName, binaryMessenger: vc.binaryMessenger)
    }
    
    func getChannel(forVC vc: FlutterViewController) -> FlutterMethodChannel{
        if flutterChannel == nil{
            self.initializeChannel(forVC: vc)
        }
        return self.flutterChannel
    }
    func channelNotifier(){
        self.flutterChannel.setMethodCallHandler { (call, result) in
            let device = UIDevice.current
            if call.method == "DEVICE_TYPE"{
                result("IOS");
            }
            else if call.method == "DEVICE_INFO"{
                result("IOS");
            }else if call.method == "DEVICE_MAKE"{
                result("Apple");
            }else if call.method == "DEVICE_MODEL"{
                result(device.model);
            }else if call.method == "DEVICE_OS_VERSION"{
                result(device.systemVersion);
            }else if call.method == "DEVICE_ID"{
                result("c21adf8e002eb6399f704a98e59cd66061acc31ab35dcb5e18f4ca478fbec0c6");
            }else if call.method == "DEVICE_NAME"{
                result(device.name)
            }else if call.method == "USER_AGENT"{
                result(Bundle.main.userAgent)
            }

        }
    }
    
}
extension Bundle {
    var shortVersion: String {
        if let version = infoDictionary?["CFBundleShortVersionString"] as? String {
            return version
        }
        return ""
    }
    
    var buildVersion: String {
        if let build = infoDictionary?["CFBundleVersion"] as? String {
            return build
        }
        return ""
    }
    
    var userAgent: String {
        let shortVersion = Bundle.main.shortVersion
        let version = Bundle.main.buildVersion
        let model = UIDevice.current.model
        let osVersion = UIDevice.current.systemVersion
        let scale = UIScreen.main.scale
        
        let userAgent = String(format: "VZM/%@/%@/%@/(%@; iOS %@; Scale/%0.2f)", shortVersion,version,model,model,osVersion,scale)
        return userAgent
    }
}
