//
//  TabBarController.swift
//  MessagePOC
//
//  Created by Lirisoft on 04/01/22.
//

import UIKit
import Flutter
import contacts_service
class TabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
           UITabBar.appearance().barTintColor = .systemBackground
           tabBar.tintColor = .label
           setupVCs()
    }
    
    func setupVCs() {
        
           viewControllers = [
            createNavController(for: MeetingsViewController(), title: "Meetings", image: UIImage(systemName: "video.fill")!),
            createNavController(for: SpeacesViewController(), title: "Speaces", image: UIImage(systemName: "globe")!),
            createFlutterController(title: "Chat", image:  UIImage(systemName: "message")!),
            contactFlutterController(title: "Contact", image: UIImage(systemName: "gear")!)
           // createNavController(for: SettingsViewController(), title: "Settings", image: UIImage(systemName: "gear")!)
           ]
       }
    
    fileprivate func createNavController(for rootViewController: UIViewController,
                                                    title: String,
                                                    image: UIImage) -> UIViewController {
          let navController = UINavigationController(rootViewController: rootViewController)
          navController.tabBarItem.title = title
          navController.tabBarItem.image = image
          navController.navigationBar.prefersLargeTitles = true
          rootViewController.navigationItem.title = title
          return navController
      }
    
    fileprivate func createFlutterController(title: String,
                                                    image: UIImage) -> UIViewController {
             
        let flutterViewController = NativeFlutterViewController(withEntrypoint: nil)
        let channelManger = FlutterChannelManager.shared
        let _ = channelManger.getChannel(forVC: flutterViewController)
        channelManger.channelNotifier()
          let navController = UINavigationController(rootViewController: flutterViewController)
          navController.tabBarItem.title = title
          navController.tabBarItem.image = image
          navController.navigationBar.prefersLargeTitles = true
          navController.setNavigationBarHidden(true, animated: false)
            flutterViewController.navigationItem.title = title
          return navController
      }
    fileprivate func contactFlutterController(title: String,
                                                    image: UIImage) -> UIViewController {
        let contactViewController = NativeFlutterViewController(withEntrypoint: "contact")
          let navController = UINavigationController(rootViewController: contactViewController)
          navController.tabBarItem.title = title
          navController.tabBarItem.image = image
          navController.navigationBar.prefersLargeTitles = true
          navController.setNavigationBarHidden(true, animated: false)
        contactViewController.navigationItem.title = title
          return navController
      }
   
}
